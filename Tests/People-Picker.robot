*** Settings ***
Documentation       People Picker tests
Resource            ../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test



########################################################################################################################
*** Test Cases ***
########################################################################################################################


########################################################################################################################
Create a new user and verify Ocean ID is found in People Picker
    [Tags]  bvt  automated data creation
    ${halyard_guest}=  Create New Guest Through Halyard
    Verify Guest ID Matches In People Picker  ${halyard_guest}


########################################################################################################################
Create a new user and verify First Name is found in People Picker
    [Tags]  bvt  automated data creation
    ${halyard_guest}=  Create New Guest Through Halyard
    Verify Guest First Name Matches In People Picker  ${halyard_guest}


########################################################################################################################
Create a new user and verify Last Name is found in People Picker
    [Tags]  bvt  automated data creation
    ${halyard_guest}=  Create New Guest Through Halyard
    Verify Guest Last Name Matches In People Picker  ${halyard_guest}



########################################################################################################################
Get All Users
    [Tags]  bulk data  makebulkdata
    ${te}  new people picker  ${env}  ${True}
    ${rv}  Call Method  ${te}  getAllUsersUsingGET
    ${200}=  Convert To String  200
    ${rtn}=  Convert To String  ${rv[0]}
    Should Contain  ${rtn}  ${200}


########################################################################################################################
Locust Prototype
    [Tags]  locust
    ${200}=  Convert To String  200
    ${rtn}=  Convert To String  200
    I Am A Locust Test
    Should Contain  ${rtn}  ${200}


########################################################################################################################
#Get Medallion Users Using GET
#    ${te}  new people picker  ${env}  ${True}
#    ${rv}  Call Method  ${te}  getMedallionUsersUsingGET  automation_test
#    ${200}=  Convert To String  200
#    ${rtn}=  Convert To String  ${rv[0]}
#    Should Contain  ${rtn}  ${200}
#    Should Contain  ${te}  ${rv}


########################################################################################################################
#Search Using GET
#    ${te}  new people picker  ${env}  ${True}
#    ${rv}  Call Method  ${te}  searchUsingGET
#    ${200}=  Convert To String  200
#    ${rtn}=  Convert To String  ${rv[0]}
#    Should Contain  ${rtn}  ${200}
#    Should Contain  ${te}  ${rv}


########################################################################################################################
#Search Using GET contains Searched Name
#    Create New User  verify=${True}  env=${env}  name=Otto Mation
#    ${te}  new people picker  ${env}
#    ${rv}  Call Method  ${te}  searchUsingGET  Otto Mation
#    Should Contain  ${rv[2]}  Otto Mation

