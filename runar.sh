#!/usr/bin/env bash

clear
git pull
pip install -U -r requirements.txt
ls -l Tests/DailyTests/*.robot
rm -f Tests/DailyTests/*.robot
rm -f *bulk_data.json
rm -rf TestResults

python ../../ve/ar/lib/python2.7/site-packages/robot/run.py --include makebulkdata --outputdir ./ar/TestResults/junk --variable env:devv ./Tests
python ../../ve/ar/lib/python2.7/site-packages/robot/run.py --include makebulkdata --outputdir ./ar/TestResults/junk --variable env:devint ./Tests
python ../../ve/ar/lib/python2.7/site-packages/robot/run.py --include makebulkdata --outputdir ./ar/TestResults/junk --variable env:qa ./Tests

python ./resources/makebulkdatatests.py
ls -l Tests/DailyTests/*.robot

python ../../ve/ar/lib/python2.7/site-packages/robot/run.py --loglevel DEBUG --exclude devint --exclude qa --outputdir ./TestResults/Devv --variable env:devv ./Tests
python ../../ve/ar/lib/python2.7/site-packages/robot/run.py --loglevel DEBUG --exclude devv --exclude qa --outputdir ./TestResults/Devint --variable env:devint ./Tests
python ../../ve/ar/lib/python2.7/site-packages/robot/run.py --loglevel DEBUG --exclude devint --exclude devint --outputdir ./TestResults/QA --variable env:qa ./Tests

