# -*- coding: utf-8 -*-
from robot.api import logger
from robot.errors import ExecutionFailed
import requests
import pprint
import json
import common


environment = common.environment
halyard_url = 'http://halyard:8080'

def __halyard(method_type, operation, payload=None):
    '''A wrapper to Halyard '''

    common.logthis("\tCreating connection to Halyard")
    common.logthis(''.join([halyard_url, operation]))

    headers = {
        'content-type': "application/json",
        'accept': "application/json",
        'cache-control': "no-cache",
    }
    common.logthis(headers)

    if payload is not None:
        d = "{    \n"
        for k, v in payload.items():
            if type(v) is list:
                d += '     "{k}": ['.format(k=str(k))
                for vi in v:
                    d += '"{v}", '.format(v=str(vi))
                d = d[:-2]
                d += '],\n'
            else:
                d += '     "{k}": "{v}",\n'.format(k=str(k), v=str(v))

        data = d[:-2] + "\n}"
        common.logthis(data)
        response = requests.request(method_type.upper(),
                                    ''.join([halyard_url, operation]),
                                    headers=headers,
                                    data=data)
    else:
        response = requests.request(method_type.upper(),
                                    ''.join([halyard_url, operation]),
                                    headers=headers)
    rf = common.pformat(response.json()).replace(r'\\n', '\n').replace(r'\\', '')
    common.logthis('Halyard response: \n' + rf)

    return response.json()


def get_guests_from_halyard():
    return __halyard('get', '/guests/{gxi_environment}'.format(gxi_environment=environment))


def post_guests_to_halyard(name=None, **kwargs):
    # set some defaults for the payload
    now_string = common.__dts
    payload = {}
    payload["age"] = "32"
    payload["anniversaryDate"] = "2015-11-12"
    payload["birthCountryCode"] = "United States"
    payload["birthDate"] = "1988-11-12"
    payload["citizenshipCountryCode"] = "US"
    payload["companyOfEmployment"] = "Level 11"
    payload["countryOfBirth"] = "United States"
    payload["countryOfCitizenship"] = "United States"
    payload["countryOfResidency"] = "United States"
    payload["emails"] = ["automatedtest{dts}@gxitestmail.com".format(dts=now_string)]
    payload["firstName"] = "Otto"
    payload["gender"] = "Male"
    payload["languagePreference"] = "eng"
    payload["lastName"] = "Mation"
    payload["maritalStatus"] = "Single"
    payload["middleName"] = now_string
    payload["nationalityCode"] = "US"
    payload["nickName"] = "Capt Auto"
    payload["placeOfBirth"] = "Seattle"
    payload["preferredName"] = "PoAdult"
    payload["retiredFlag"] = "N"
    payload["suffix"] = "Mr"
    payload["title"] = "SDET"
    payload["wheelchairType"] = "NotConfined"

    if kwargs:
        # replace values that may hae been passed in via kwargs
        for k, v in kwargs['kwargs'].items():
            payload[k] = v
            common.logthis(k + ": " + v)
            if k == 'name':
                payload['first_name'] = v.split()[0]
                payload['last_name'] = str(v.split()[:-1])
                if len(v.split()[1]) == 1:
                    payload['middle'] = v.split()[1]

    if name is not None:
        payload['first_name'] = name.split()[0]
        payload['last_name'] = str(name.split()[:-1])
        if len(name.split()[1]) == 1:
            payload['middle'] = name.split()[1]

    # post /guests/{gxi_environment}
    rv = __halyard('post', '/guests/{env}'.format(env=environment), payload=payload)
    if 'error' in str(rv):
        common.logthis("Error encountered by Halyard" + str(rv))
        raise ExecutionFailed(message="Error encountered by Halyard")
    return rv


def create_new_guest_through_halyard(name=None):
    return post_guests_to_halyard(name)


def delete_guests_relationships_through_halyard():
    # /guests/{gxi_environment}/relationships
    # payload
    #     {
    #     "relationshipType": "string",
    #     "users": [
    #         {
    #             "toUiid": "string",
    #             "fromUiid": "string"
    #         }
    #     ]
    # }

    pass


def put_guests_relationships_of_relationship_type_through_halyard():
    # /guests/{gxi_environment}/relationships/{relationship_type}
    # relationship type to create. Any of the following values: Contact, TravelCompanion
    # 
    pass

def get_guests_by_guest_id_through_halyard():
    pass

def post_events_through_halyard():
    pass

def get_events_categories_through_halyard():
    pass

def get_journeys_through_halyard():
    pass

def post_journeys_through_halyard():
    pass

def get_scheduled_events_through_halyard():
    pass

def post_scheduled_events_through_halyard():
    pass




