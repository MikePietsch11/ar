import os
import json
import pprint
import requests
import templatestrings

if os.sep == '/':
    BULK_TESTS_DIR = os.sep.join([os.path.abspath(os.path.join(os.getcwd(), os.pardir)), "ar", "Tests", "DailyTests"])
else:
    BULK_TESTS_DIR = os.sep.join(['c:', 'code', 'ar', "Tests", "DailyTests"])

BULK_TESTS_FILE = os.sep.join([BULK_TESTS_DIR, "Bulk Data Tests.robot"])

#  Not sure where these will end up on a mac
envs = (['devint', os.getcwd() + os.sep + "devint_bulk_data.json"],
        ['devv', os.getcwd() + os.sep + "devv_bulk_data.json"],
        ['qa', os.getcwd() + os.sep + "qa_bulk_data.json"])


class PP:
    def __init__(self, env='devv'):
        self.url = "http://peoplepicker-svc_8088.{env}.gxicloud.com:9999".format(env=env)
        self.env = env
        self.headers = {
            'accept': "application/json",
            'cache-control': "no-cache",
        }

    def get_user_details(self, uiid):
        url = ''.join([self.url, '/v1/social/pp/users/details'])
        querystring = {"user-ids": "{}".format(uiid)}
        response = requests.request("GET", url, headers=self.headers, params=querystring)
        pprint.pprint(response.json())
        return response


def format_docs(docs):
    lines = docs.split('\n')
    rv = ''
    for i in range(len(lines)):
        if i == 0:
            rv = " " + lines[i] + "${\\n}" + '\n'
        else:
            rv += '    ...              ' + lines[i] + "${\\n}" + '\n'
    return rv


def pp200():
    ppdoc = format_docs('''People picker documentation\n''')
    rv = templatestrings.people_picker_200.replace('{pp2rdoc}', ppdoc)
    return rv


def check_user_details(doc):
    rv = templatestrings.check_user_details. \
        replace('{cudndoc}',
                format_docs('''Check User Details (first or last)\n''' +
                            pprint.pformat(doc)))
    return rv


def check_user_details_middle(doc):
    rv = templatestrings.check_user_details_middle. \
        replace('{cudmndoc}',
                format_docs('''Check User Details Middle\n''' +
                            pprint.pformat(doc)))
    return rv


def no_contacts(doc):
    rv = templatestrings.no_contacts. \
        replace('{nvdoc}',
                format_docs('''No Contacts\n''' +
                            pprint.pformat(doc)))
    return rv


def some_contacts(doc):
    rv = templatestrings.some_contacts. \
        replace('{scdoc}',
                format_docs('''Some Contacts\n''' +
                            pprint.pformat(doc)))
    return rv


def user_does_not_exist(doc):
    rv = templatestrings.user_does_not_exist. \
        replace('{udnedoc}',
                format_docs('''User Does Not Exist\n''' +
                            pprint.pformat(doc)))
    return rv


for env in envs:
    print(env)
    try:
        with open(env[1], 'r') as json_data:
            d = json.load(json_data)

        pprint.pprint(len(d))
        # pprint.pprint(d)

        for r in d:
            # fname = ''
            try:
                u = r['userId']
                f = r['personalDetails']['firstName'] if r['personalDetails']['firstName'] is not '' else ''
                m = r['personalDetails']['middleName'] if r['personalDetails']['middleName'] is not '' else ''
                l = r['personalDetails']['lastName'] if r['personalDetails']['lastName'] is not '' else ''
                e = r['personalDetails']['emails'][0] if r['personalDetails']['emails'][0] is not '' else ''

                relationships = r['relationships']
                fname = os.sep.join([BULK_TESTS_DIR, "{t}.robot".format(t=' '.join([f, m, l]))]).replace('\\\\', '\\')

                user_docs = pprint.pformat(r)
                user_doc = '...                 <pre>'
                for line in user_docs.split('\n'):
                    user_doc += '\n...                 ' + line + r'${\n}'
                user_doc += '...                 </pre>'

                tf = templatestrings.header.replace('{user_documentation}', user_doc)

                tf += pp200()
                tf += check_user_details(r)
                if m is not '':
                    tf += check_user_details_middle(r)

                # conditional tests
                if len(relationships) == 0:
                    tf += no_contacts(r)
                else:
                    tf += some_contacts(relationships)
                    pp = PP(env=env[0])

                    userR = ''
                    for relation in relationships:
                        _from = relation['fromElement']['entityId']
                        _to = relation['toElement']['entityId']
                        r = [_from, _to]
                        if u in r:
                            r.remove(u)
                        userR = r[0]

                # tf += no_users_nearby
                # tf += no_recommended_users
                # tf += some_recommended_users
                # tf += reservation_party_solo
                # tf += reservation_party_molto
                # tf += travel_companions_one_to_one
                # tf += travel_companions_one_to_one_group
                # tf += travel_companions_two_tc_groups
                # tf += travel_companions_unrequited
                # tf += no_parties
                tf += user_does_not_exist(r)

                replace_these = {'{fn}': f,
                                 '{m}': m,
                                 '{ln}': l,
                                 '{oid}': u,
                                 '{env}': env[0],
                                 '{em}': e,
                                 '${userR}': "uiid={}".format(userR),
                                 '{revu}': str(''.join(reversed(u)))
                                 # '{revu}': 'Bob'
                                 }

                for replace_k, replace_v in replace_these.items():
                    tf = tf.replace(replace_k, replace_v)

                # print(''.join(['\n', '*' * 80]))
                pprint.pprint(fname)
                with open(str(fname), 'w') as test_file:
                    test_file.writelines(tf)
                # pprint.pprint(tf)

            except Exception as e:
                print(e)

    except Exception as e:
        print(e)
