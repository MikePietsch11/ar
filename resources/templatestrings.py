# has no contacts, groups, nothing
#&{userA}    uiid=oceanId-2176a647-8730-4abd-9290-9ff516df4fef   email=bvtuseraa@gxitestmail.com  name=Alice

# has contacts with D and F
#&{userB}    uiid=oceanId-83050041-f169-448d-9844-0af4fc776df4   email=bvtuserbb@gxitestmail.com  name=Bob
#&{userC}    uiid=oceanId-7b6aff5b-0748-4a98-a1f4-330f8160196f   email=bvtuserc@gxitestmail.com  name=Carol
#&{userD}    uiid=oceanId-610604b3-9a35-44c2-8871-394e0f8d1a6d   email=bvtuserd@gxitestmail.com  name=David
#&{userE}    uiid=oceanId-943cec6d-82f0-428e-93fd-fb11ff97e27e   email=bvtusere@gxitestmail.com  name=Emily

# has recommendation for G
#&{userF}    uiid=oceanId-45ebad64-fe4b-466e-a3c3-36d176417189   email=bvtuserf@gxitestmail.com  name=Fred
#&{userG}    uiid=oceanId-27140cac-e46e-4bd2-aa81-1dd2799c19ee   email=bvtuserg@gxitestmail.com  name=Gail
#&{userH}    uiid=oceanId-0128a71b-5248-473d-ac48-8e636c66eff0   email=bvtuserh@gxitestmail.com  name=Howard
#&{userI}    uiid=oceanId-b7e9f236-0de6-4448-8c1b-b97e274a36cb   email=bvtuseri@gxitestmail.com  name=Ingrid
#&{userJ}    uiid=oceanId-20aeff01-9e0c-4934-856e-22f02e1f9349   email=bvtuserj@gxitestmail.com  name=John



header = '''*** Settings ***
Documentation       Tests in this suite are created through command line:
...                 python makebulkdatatests
...                 {user_documentation}

Resource            ../../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test


########################################################################################################################
*** Test Cases ***
########################################################################################################################

'''


people_picker_200 = '''
########################################################################################################################
People Picker 200 response {fn} {ln}
    [Documentation]  {pp2rdoc}
    [Tags]  People Picker 200 response  Bulk Data  {env}
    ${r}=  people controller getUserDetailsUsingGET  environ={env}  ocean_id={oid}
    ${r_str}=  Convert To String  ${r}
    Should Contain  ${r_str}  [200]


'''

check_user_details = '''
########################################################################################################################
Check User Details First Name for {fn} {ln}
    [Documentation]  {cudndoc}
    [Tags]  Check User Details First Name  Bulk Data  {env}
    verify people picker response data  environ={env}  ocean_id={oid}  k=firstName  v={fn}


########################################################################################################################
Check User Details Last Name for {fn} {ln}
    [Documentation]  {cudndoc}
    [Tags]  Check User Details Last Name  Bulk Data  {env}
    verify people picker response data  environ={env}  ocean_id={oid}  k=lastName  v={ln}


'''

check_user_details_middle = '''
########################################################################################################################
Check User Details Middle Name for {fn} {ln}
    [Documentation]  {cudmndoc}
    [Tags]  Check User Details Middle Name  Bulk Data  {env}
    verify people picker response data  environ={env}  ocean_id={oid}  k=middleName  v={m}


'''

no_contacts = '''
########################################################################################################################
No Contacts for {fn} {ln}
    [Documentation]  {ncdoc}
    [Tags]  No Contacts  Bulk Data  {env}
    &{userA}=  Create Dictionary  uiid={oid}  email={em}  name={fn}
    ${json}=   Get Contacts For  &{userA}
    ${userlist}=  Set Variable  ${json[0]["users"]}
    Should Be Empty  ${userlist}


'''


some_contacts = '''
########################################################################################################################
Some Contacts for {fn} {ln}
    [Documentation]  {scdoc}
    #  is a contact the same as a relationship?  What type of relationship?
    
    [Tags]  Some Contacts  Bulk Data  {env}
    &{userB}=  Create Dictionary  uiid={oid}  email={em}  name={fn}
    ${json}=    Get Contacts For    &{userB}
    Should Not Be Empty     ${json}
    ${userlist}=    Set Variable    ${json[0]["users"]}
    User Should Be In Result Set    ${userlist}     ${userR}
    
    
    #  Users Should Be In Result Set    ${userlist}     ${userF}   ${userI}
    
    # This is a negative test case and should be tested elsewhere
    # Users Should Not Be In Result Set   ${userlist}     ${userD}


'''

no_users_nearby = '''
########################################################################################################################
No Users Nearby for {fn} {ln}
    [Documentation]  {nundoc}
    [Tags]  No Users Nearby  Bulk Data  {env}
    ${json}=    Get PeoplePicker    nearby    ${userA}
    ${list}=    Get Userlist From Result    ${json}
    Length Should Be    ${list}     0


'''

no_recommended_users = '''
No Recommended Users for {fn} {ln}
    [Tags]  No Recommended Users  Bulk Data  {env}
    ${json}=    Get PeoplePicker    recommended     ${userA}
    ${list}=    Get Userlist From Result    ${json}
    Length Should Be    ${list}     0


'''

some_recommended_users = '''
Some Recommended Users for {fn} {ln}
    [Tags]  Some Recommended Users  Bulk Data  {env}
    ${json}=    Get PeoplePicker    recommended     ${userB}
    ${list}=    Get Userlist From Result    ${json}
    Users Should Be In Result Set   ${list}     ${userI}


'''


reservation_party_solo = '''
Reservation Party Solo for {fn} {ln}
    [Tags]  Reservation Party Solo  Bulk Data  {env}
    ${json}=    Get PeoplePicker    resparty     ${userB}
    Users Should Not Be In Result Set    ${json}     ${userC}    ${userD}    ${userE}   ${userF}


'''

reservation_party_molto = '''
Reservation Party Molto for {fn} {ln}
    [Tags]  Reservation Party Molto  Bulk Data  {env}
    ${json}=    Get PeoplePicker    resparty     ${userD}
    ${userset}=     Get Userlist From Result    ${json}
    Users Should Be In Result Set    ${userset}     ${userC}
    Users Should Not Be In Result Set    ${userset}    ${userE}   ${userF}


'''


travel_companions_one_to_one = '''
Travel Companions One To One for {fn} {ln}
    [Tags]  Travel Companions One To One  Bulk Data  {env}
    ${json}=    Get Travel Companions   ${userI}
    ${userset}=     Get Userlist From Result    ${json}
    Users Should Be In Result Set   ${userset}  ${userJ}


'''


travel_companions_one_to_one_group = '''
Travel Companions One To One Group for {fn} {ln}
    [Tags]  Travel Companions One To One Group  Bulk Data  {env}
    ${jsonB}=    Get Travel Companions     ${userB}
    ${usersetB}=     Get Userlist From Result    ${jsonB}
    Users Should Be In Result Set  ${usersetB}    ${userC}     ${userD}
    Users Should Not Be In Result Set   ${usersetB}    ${userE}    ${userF}    ${userG}
    ${jsonC}=   Get Travel Companions   ${userC}
    ${usersetC}=    Get Userlist From Result    ${jsonC}
    Users Should Be In Result Set   ${usersetC}     ${userD}    ${userB}



'''

travel_companions_two_tc_groups = '''
Travel Companions Two TC Groups for {fn} {ln}
    [Tags]  Travel Companions Two TC Groups  Bulk Data  {env}
    ${jsonC}=   Get Travel Companions     ${userC}
    ${usersetC}=     Get Userlist From Result    ${jsonC}
    Users Should Be In Result Set  ${usersetC}    ${userD}     ${userE}    ${userF}
    Users Should Not Be In Result Set   ${usersetC}    ${userG}   ${userH}
    ${jsonD}=   Get Travel Companions     ${userD}
    ${usersetD}=     Get Userlist From Result    ${jsonD}
    # there should be userC as well but I buggered the data :(
    Users Should Be In Result Set  ${usersetD}         ${userE}    ${userF}
    Users Should Not Be In Result Set   ${usersetD}    ${userG}   ${userH}


'''


travel_companions_unrequited = '''
Travel Companions Unrequited for {fn} {ln}
    [Tags]  Travel Companions Unrequited  Bulk Data  {env}
    ${jsonE}=   Get Travel Companions   ${userE}
    ${usersetE}=     Get Userlist From Result    ${jsonE}
    Users Should Not Be In Result Set   ${usersetE}    ${userG}    ${userH}
    ${jsonF}=   Get Travel Companions   ${userF}
    ${usersetF}=     Get Userlist From Result    ${jsonF}
    Users Should Not Be In Result Set   ${usersetF}    ${userG}    ${userH}
#    ${jsonG}=   Get Travel Companions   ${userG}
#    ${usersetG}=     Get Userlist From Result    ${jsonG}
#    Users Should Not Be In Result Set    ${usersetG}    ${userE}    ${userF}    ${userC}    ${userD}


'''

no_parties = '''
No Parties for {fn} {ln}
    [Tags]  No Parties  Bulk Data  {env}
    ${json}=    Get PeoplePicker    resparty  ${userA}
    ${userlist}=    Get Userlist From Result  ${json}
    Should Be Empty     ${userlist}     User should have no reservation party
    ${json2}=   Get PeoplePicker    travelparty     ${userA}
    ${userlist2}=    Get Userlist From Result  ${json2}
    Should Be Empty     ${userlist2}    User should have no travel party


'''

user_does_not_exist = '''
########################################################################################################################
User Does Not Exist (recommended) for {fn} {ln}
    [Documentation]  {udnedoc}
    [Tags]  User Does Not Exist  Bulk Data  {env}
    ${hcode}=  Set Variable  200
    ${userX}=  Create Dictionary  uiid={revu}
    ${out}=  Get PeoplePicker  recommended  ${userX}  status=${hcode}
    ${list}=  Get Userlist From Result  ${out}
    Length Should Be    ${list}     0
    ${out}=  Get PeoplePicker  details  ${userX}  status=${hcode}  raw=True
    Should Be Empty  ${out}


########################################################################################################################
User Does Not Exist (travelparty) for {fn} {ln}
    [Documentation]  {udnedoc}
    [Tags]  User Does Not Exist  User Does Not Exist (travelparty)  Bulk Data  {env}
    ${hcode}=  Set Variable  200
    ${userX}=  Create Dictionary  uiid={revu}
    ${out}=  Get PeoplePicker  travelparty  ${userX}  status=${hcode}
    ${list}=  Get Userlist From Result  ${out}
    Length Should Be    ${list}     0
    ${out}=  Get PeoplePicker  details  ${userX}  status=${hcode}  raw=True
    Should Be Empty  ${out}


########################################################################################################################
User Does Not Exist (resparty) for {fn} {ln}
    [Documentation]  {udnedoc}
    [Tags]  User Does Not Exist  User Does Not Exist (resparty)  Bulk Data  {env}
    ${hcode}=  Set Variable  200
    ${userX}=  Create Dictionary  uiid={revu}
    ${out}=  Get PeoplePicker  resparty  ${userX}  status=${hcode}
    ${list}=  Get Userlist From Result  ${out}
    Length Should Be    ${list}     0
    ${out}=  Get PeoplePicker  details  ${userX}  status=${hcode}  raw=True
    Should Be Empty  ${out}


########################################################################################################################
User Does Not Exist (nearby) for {fn} {ln}
    [Documentation]  {udnedoc}
    [Tags]  User Does Not Exist  User Does Not Exist (nearby)  Bulk Data  {env}
    ${hcode}=  Set Variable  200
    ${userX}=  Create Dictionary  uiid={revu}
    ${out}=  Get PeoplePicker  nearby  ${userX}  status=${hcode}
    ${list}=  Get Userlist From Result  ${out}
    Length Should Be    ${list}     0
    ${out}=  Get PeoplePicker  details  ${userX}  status=${hcode}  raw=True
    Should Be Empty  ${out}



########################################################################################################################
User Does Not Exist (contacts) for {fn} {ln}
    [Documentation]  {udnedoc}
    [Tags]  User Does Not Exist  User Does Not Exist (contacts)  Bulk Data  {env}
    ${hcode}=  Set Variable  200
    ${userX}=  Create Dictionary  uiid={revu}
    ${out}=  Get PeoplePicker  contacts  ${userX}  status=${hcode}
    ${list}=  Get Userlist From Result  ${out}
    Length Should Be    ${list}     0
    ${out}=  Get PeoplePicker  details  ${userX}  status=${hcode}  raw=True
    Should Be Empty  ${out}


'''

