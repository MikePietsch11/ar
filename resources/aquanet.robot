########################################################################################################################
*** Settings ***
########################################################################################################################
Library         aquanet.py
Library         halyard.py
Library         locustfile.py
Library         OperatingSystem
Library         RequestsLibrary
Library         Collections

Variables       aquanet.py


########################################################################################################################
*** Variables ***
########################################################################################################################
${env}=  devv           # will/should be overwritten via command line --varable env:abc
#${locust}=  ${None}


########################################################################################################################
*** Keywords ***
########################################################################################################################
Startup Test
    [Documentation]  This is the startup routine for every test
    fig title  ${TEST NAME}
#    run keyword if  ${locust}==${True}
    Log Variables


Shutdown Test
    [Documentation]  This is the shutdown routine for every test
    output test status  ${TEST STATUS}


Verify Response is 200
    [Arguments]  ${response}
    ${r}  Convert To String  ${response}
    ${v}  Convert To String  200
    should be equal as numbers  ${r}  ${v}


Verify Response Contains
    [Arguments]  ${response}  ${name}
    ${str_response}=  Convert To String  ${response}
    should contain  ${str_response}  ${name}


Get Contacts For
    [Arguments]  &{user}
    Run Keyword And Return  Get PeoplePicker  contacts  ${user}  extract=True

Get Userlist From Result
    [Arguments]     ${result}
    [Return]    ${result["data"][0]["users"]}


Get PeoplePicker
    [Documentation]     A wrapper for making PP calls.
    ...                 First argument is the specific PP call, usually the portion of the
    ...                 URL after the UID. Second argument is the UID.
	[Arguments]  ${resource}  ${userobj}  ${status}=200  ${raw}=False  ${extract}=False
	${correlationId}=  Evaluate  str(uuid.uuid1())  modules=uuid
	Log  CORRELATION ${correlationId}
	Log To Console  CORRELATION ${correlationId}
	${headers}=  Create Dictionary  x-correlation-id=${correlationId}
	${session}=  Create Session  people_picker  ${peoplepickerurl}
	${uid}=	 Set Variable  &{userobj}[uiid]
	log  ${peoplepickerurl}/v1/social/pp/users/${uid}/${resource}
	log to console  ${peoplepickerurl}/v1/social/pp/users/${uid}/${resource}
	${response}=  Get Request  people_picker  /v1/social/pp/users/${uid}/${resource}  headers=${headers}
    log  ${response.text}
	log to console  ${response.text}
	Should Be Equal As Strings  ${response.status_code}	 ${status}  Expected HTTP ${status}, instead got ${response.status_code}  values=false
    ${returnval}=  Set Variable If  ${raw}==True    ${response.content}   ${response.json()}
    ${returnval}=  Set Variable If  ${extract}==True    ${returnval["data"]}    ${returnval}
	[Return]  ${returnval}


Users Should Be In Result Set
    [Arguments]     ${set}  @{users}
    :FOR    ${user}     in  @{users}
    \   User Should Be In Result Set    ${set}  &{user}


User Should Be In Result Set
    [Arguments]     ${resultset}    &{user}
    ${resultIsLOD}=    Evaluate    isinstance(${resultset}[0],dict)
    #Run Keyword If  ${resultIsLOD}   Log To Console  Result set is ListOfDictionaries
    Run Keyword If  ${resultIsLOD}   List Of Dictionaries Should Contain Item    ${resultset}    userId  ${user["uiid"]}
    #Run Keyword Unless  ${resultIsLOD}  Log To Console  Result set is simple list of values
    Run Keyword Unless  ${resultIsLOD}     List Should Contain Value   ${resultset}    ${user["uiid"]}


List Of Dictionaries Should Contain Item
    [Arguments]     ${dictlist}   ${key}  ${value}
    ${found}=   Set Variable    ${FALSE}
    :FOR    ${dict}     IN      @{dictlist}
    \   ${found}=   Run Keyword And Return Status   Dictionary Should Contain Item   ${dict}     ${key}  ${value}
    \   Exit For Loop If    ${found}
    Run Keyword Unless  ${found}    Fail    No dictionary in list contained "${key}": "${value}"

