# -*- coding: utf-8 -*-

from pyfiglet import Figlet
from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn
from robot.libraries.BuiltIn import RobotNotRunningError
# from robot.errors import RobotError
import pprint
import datetime


__dts = str(datetime.datetime.now()).replace(" ", "_").replace(":", "_").replace("-", "_").replace(".", "_")


def logthis(message):
    try:
        if type(message) is dict:
            logger.console(pprint.pformat(message))
            logger.info(message)
        else:
            logger.console(message)
            logger.info(message)
    except Exception as e:
        logger.console(pprint.pformat(message))
        logger.console(e)


def global_var(var_name):
    rv = BuiltIn().get_variable_value("${" + var_name + "}")
    return rv


def fig_title(title):
    '''used to print the test title with figlet so it is obvious what is running
    :return: none
    '''
    logger.console('')
    # create the object
    f = Figlet("cybermedium")
    logger.console(f.renderText(title))
    logger.console(title)
    fig_env()


def fig_env():
    '''used to print the test title with figlet so it is obvious what is running
    :return: none
    '''
    logger.console('')

    # create the object
    f = Figlet("bubble")
    logger.console(f.renderText(environment))
    logger.console("Test environment: {}".format(environment))


def output_test_status(status_message):
    '''outputs the status of the test backwards to the console'''
    f = Figlet()
    logger.console('')
    logger.console(f.renderText(status_message))


def pformat(message):
    return pprint.pformat(message)

try:
    environment = global_var('env')
except RobotNotRunningError as e:
    environment = "devint"
