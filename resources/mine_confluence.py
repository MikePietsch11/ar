import subprocess
import shlex
from bs4 import BeautifulSoup as bs
import os
import pandas as pd
from pprint import pprint
from tabulate import tabulate

def run_command(command, show=False):
    full_output = ''
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            if show:
                print output.strip()
            full_output += output.strip()
    rc = process.poll()
    return rc, full_output

cl = 'python.exe confluence.py ' \
     '-w https://carnival.atlassian.net/wiki ' \
     '-u {n} -p {p} ' \
     '-v getpagecontent ' \
     '-n "Crew Data" ' \
     '-s TPA'.\
    format(n=os.environ['carnivalatlassianname'], p=os.environ['carnivalatlassianpwd'])

rco = run_command(cl)[1]

soup = bs(rco, 'lxml')

found_tables = soup.find_all('table')
tables = {
    'devint': found_tables[0],
    'qa':  found_tables[1]
}

for table, table_html in tables.items():
    print(table)

# for table in tables:
#     headings = table.find_all('th')
#     print(len(headings))
#
#     if len(headings) < 1 :
#         pprint(table)

# for row in soup.table.find_all('tr'):
#     cells = row.find_all("td")
#     # rn = cells[0].get_text()
#     for cell in cells:
#         print(cell.get_text())
#

# for table in tables:
#     df = pd.read_html(str(table))
    # pprint(df[0].to_json())
    # ocean_ids = df["uiid"]
    # ocean_ids = df[3]
    # usernames = df['username'].tolist()
    # usernames = df[5].tolist()
    # passwords = df['Password'].tolist()
    # passwords = df[7].tolist()
    # print(df.keys())
    # print(ocean_ids)
    # print (tabulate(df[0], headers='keys', tablefmt='psql'))
    # print (tabulate(df))

    # for i, (ocean_id, name, password) in enumerate(zip(ocean_ids, usernames, passwords)):
    #     print([i, ocean_id, name, password])

