# -*- coding: utf-8 -*-

import requests
import locust
from locust import HttpLocust
from locust import task
from locust import TaskSet
from robot.api.logger import console
import time
import pprint
import aquanet


class MyTaskSet(TaskSet):
    sort_by_name = False

    @staticmethod
    def output(text):
        try:
            console(text)
        except:
            print(text)

    def on_start(self):
        import requests

        url = "https://am.iam-devint.gxicloud.com/openam/oauth2/access_token"

        querystring = {"realm": "/ocean/guests"}

        payload = "grant_type=password&username=GUEST_USERNAME&password=GUEST_PASSWORD"
        headers = {
            'authorization': "Basic U29jaWFsU2VydmljZTpDb2xsYWJvcjgh",
            'content-type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache",
            'postman-token': "3699907d-8e6d-5248-299a-7141442929be"
        }

        response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

        print(response.text)

    def my_name(self):
        """ cleans up and returns the address of the locust """
        # <locustfile.MyLocust object at
        n = str(self.locust).replace('<locustfile.MyLocust object at', '').replace('>', '')
        return n

    def json_test(self, url, name, task_name, **kwargs):
        """ json_test method supports task methods for json test site"""
        json_response = dict()
        n = name if self.sort_by_name else name.replace(name, '')

        with self.client.get(url, name=n + " " + task_name, catch_response=True, **kwargs) as response:
            try:
                json_response = response.json()
                self.output(name + '= ' + str(response.json()))
            except ValueError as e:
                self.output(e.message)
                self.output(response.text)
                response.failure("Could not parse json response")
                return

            response.success()

    # @task
    def ip(self):
        """ returns the ip of the requester """
        url = 'http://ip.jsontest.com/'
        self.json_test(url, self.my_name(), 'ip')

    # @task
    def headers(self):
        """ returns requesters headers """
        url = 'http://headers.jsontest.com/'
        self.json_test(url, self.my_name(), 'headers')

    # @task
    def date(self):
        """  returns the data of the request """
        url = 'http://date.jsontest.com'
        self.json_test(url, self.my_name(), 'date')

    # @task
    def time(self):
        """ returns time of request """
        url = 'http://time.jsontest.com'
        self.json_test(url=url, name=self.my_name(),  task_name='time')

    @task
    def user_details_using_get(self):
        """ yet another attempt to talk to people picker """
        name = self.my_name() + ' user details using get'
        url = "http://peoplepicker-svc_8088.devint.gxicloud.com:9999/v1/social/pp/users/details"
        headers = {
            'accept': "application/json",
            'cache-control': "no-cache",
        }
        querystring = {"user-ids": "{}".format("[ocean-id-7aad1be0-31d3-47be-a5ed-8abdbd889816]")}
        self.json_test(url=url,
                       name=str(self.my_name()),
                       task_name="user details",
                       headers=headers,
                       params=querystring)

    @task
    def PP_Robot_user_details_using_get(self):
        """ yet another attempt to talk to people picker """
        name = 'aquanet PP'
        with self.client(aquanet.people_controller_getUserDetailsUsingGET(
                'devint',
                ocean_id='ocean-id-7aad1be0-31d3-47be-a5ed-8abdbd889816'))as response:
            try:
                json_response = response.json()
                self.output(name + '= ' + str(response.json()))
            except ValueError as e:
                self.output(e.message)
                self.output(response.text)
                response.failure("Could not parse json response")
                return

            response.success()


class MyLocust(HttpLocust):
    task_set = MyTaskSet

    min_wait = 5000         # 5 second minimum wait
    max_wait = 15000        #15 seconds maximum wait
    host = 'http://'


def i_am_a_locust_test():
    console("I am green and freaky, who am I?")
    locust_server = MyLocust()
    locust_server.stop_timeout = 3 * 60
    locust_server.run()

    for lc in range(30, 0, -1):
        payload = {
            'locust_count': lc,
            'hatch_rate': 10,
            'num_requests': 3,
        }

        response = requests.post("http://127.0.0.1:8089/swarm", params=payload)
        console(response.json())
        console(lc)
        time.sleep(1)


if __name__ == '__main__':
    import requests

    url = "https://am.iam-devint.gxicloud.com/openam/oauth2/access_token"

    querystring = {"realm": "/ocean/guests"}

    payload = "grant_type=password&username=GUEST_USERNAME&password=GUEST_PASSWORD"
    headers = {
        'authorization': "Basic U29jaWFsU2VydmljZTpDb2xsYWJvcjgh",
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
        'postman-token': "3699907d-8e6d-5248-299a-7141442929be"
    }

    response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

    print(response.text)