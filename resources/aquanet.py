# -*- coding: utf-8 -*-

# from robot.libraries.BuiltIn import RobotNotRunningError
from robot.errors import ExecutionFailed
from robot.api import logger
import robot
import datetime
import requests
import collections
import pprint
import json
import locustfile
from os import path
from common import fig_env
from common import fig_title
from common import output_test_status
from common import logthis
from common import environment

# Setup some global variables for robot tests
# environment = global_var('env')
peoplepickerurl = "http://peoplepicker-svc_8088.{env}.gxicloud.com:9999".format(env=environment)


class CreateUser:
    uiid = None
    __options = {'env': 'devint',
                 'unique': True,
                 'name': 'auto_test',
                 'first_name': 'Otto',
                 'middle': 'G',
                 'last_name': 'Mation',
                 'password': 'auto_test'
                 }
    __admin_token = None
    __protocol = "https"
    __openam_admin = "te2-admin"
    __openam_p = "P@$$w0rd5"
    __version = "v1"
    __context = ""
    __captured_uiid = None
    __response = {}

    # "host": "te2-proxy-devint.gxicloud.com:8443",
    # "activity_host": "activity-entity-devint.gxicloud.com:8443",
    # "guest_host": "guest-entity-devint.gxicloud.com:8443",
    # "journey_host": "guest-journey-devint.gxicloud.com:8443",
    # "excursion_host": "excursion-entity-devint.gxicloud.com:8443",
    # "location_host": "user-location-devint.gxicloud.com:8443",
    # "recommendations_host": "recommendation-task-devint.gxicloud.com:8443",
    # "servicemix_host": "servicemix-devint.gxicloud.com:8443",
    # "eventsub_host": "event-subscription-devint.gxicloud.com:8443",
    # "stream_host": "activity-stream-devint.gxicloud.com:8443",
    # "true_north_host": "true-north-devint.gxicloud.com:8443",
    # "mock_host": "wiremock-devint.gxicloud.com:8443",
    # "commerce_facade": "commercefacade-devint.gxicloud.com:8443",

    def __init__(self, **kwargs):
        logthis("Creating new user")
        if kwargs:
            logthis("Setting creation options")
            for k, v in kwargs['kwargs'].items():
                self.__options[k] = v
                logthis(k + ": " + v)
                if k == 'name':
                    self.__options['first_name'] = v.split()[0]
                    self.__options['last_name'] = str(v.split()[:-1])
                    if len(v.split()[1]) == 1:
                        self.__options['middle'] = v.split()[1]
        if not self.__options['unique']:
            self.__username = self.__username.replace("_" + self.__dts, "")
        self.__options = self.__options
        self.__dts = str(datetime.datetime.now()).replace(" ", "_").replace(":", "_").replace("-", "_").replace(".",
                                                                                                                "_")
        self.__username = "{name}_{dts}".format(name=self.__options['name'].replace(" ", "_"), dts=self.__dts)
        self.__email = self.__username + "@gxitestmail.com"
        self.__openam_host = "am.iam-{}.gxicloud.com".format(self.__options['env'])
        self.__auth_host = "auth-task-{}.gxicloud.com:8443".format(self.__options['env'])
        self.__identity_host = "identity-hub-{}.gxicloud.com:8443".format(self.__options['env'])

        self.url = "{protocol}://{openam_host}/".format(
            protocol=self.__protocol,
            openam_host=self.__openam_host
        )

        self.options = self.__options
        self.options['password'] = "*" * len(self.options['password'])

    def __str__(self):
        rv = "\n".join(["",
                        "USER INFORMATION",
                        "\tUser Name: " + self.__username,
                        "\tUser Password: " + self.__options['password'],
                        "\temail: " + self.__email])
        rv += "\nCreation options"
        rv += pprint.pformat(self.__options)
        rv += "\nValues from API's"
        for k, v in self.__response.items():
            if type(v) == 'list':
                rv += "\n\t" + pprint.pformat(k)
                for l in v:
                    rv += "\n\t" + pprint.pformat(l)
            else:
                rv += "\n\t" + str(k) + "\t" + pprint.pformat(v)
        return rv

    def execute(self):
        # create the user
        # see: https://carnival.atlassian.net/wiki/display/EEQA/Automating+Ocean+Account+Creation
        self.__set_open_am_admin_token()
        self.__create_open_am_credentials()
        self.__create_guest_profile_and_get_uiid()
        self.__link_uiid_to_username()

        self.uiid = self.__captured_uiid

    def __set_open_am_admin_token(self):
        logthis("\tSetting OpenAM admin token")

        url = self.url + "openam/json/ocean/guests/authenticate"
        payload = "{}"
        headers = {
            'x-openam-username': self.__openam_admin,
            'x-openam-password': self.__openam_p,
            'content-type': "application/json",
            'cache-control': "no-cache",
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        self.__admin_token = response.json()['tokenId']
        logthis("OpenAM token:\t" + self.__admin_token)
        for k, v in response.json().items():
            val = '__' + str(k)
            self.__response[val] = v
        logthis(pprint.pprint(response.json()))
        return response.json()['tokenId']

    def __create_open_am_credentials(self):
        logthis("\tCreating Open AM Credentials")
        url = self.url + "/openam/json/ocean/guests/users/"
        querystring = {"_action": "create"}
        payload = '{\n'
        payload += '     "username": "{u}",\n'.format(u=self.__username)
        payload += '     "userpassword": "{p}",\n'.format(p=self.__options['password'])
        payload += '     "mail": "{e}"\n'.format(e=self.__email)
        payload += "}"
        headers = {
            'content-type': "application/json",
            'iplanetdirectorypro': self.__admin_token,
            'cache-control': "no-cache",
        }
        response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
        for k, v in response.json().items():
            val = '__' + str(k)
            self.__response[val] = v

    def __create_guest_profile_and_get_uiid(self):
        logthis("\tCreating guest profile and getting UIID")
        if len(self.__context) > 1:
            url = self.url.replace(self.__openam_host, self.__auth_host) + \
                  self.__context + "/" + self.__version + '/auth-task/users?email={e}'. \
                      format(e=self.__email)
        else:
            url = self.url.replace(self.__openam_host, self.__auth_host) + \
                  self.__version + '/auth-task/users?email={e}'.format(e=self.__email)

        querystring = {"email": "{e}".format(e=self.__email)}

        thisiswhatthesshas = '''
        username	andrew.postman.ten
        password	!Working123
        brandCRMId	po-ccn-10
        suffixTitle	Mr
        firstName	Andrew
        lastName	Polaskey
        preferredName	PoChild
        nickName	Po
        email	andrew.postman.ten@gxitestmail.com
        gender	Male
        age	11
        languagePreference	eng
        wheelChairType	Onboard
        retiredFlag	N
        maritalStatus	Single
        anniversaryDate	11/12/2015
        birthCountryCode	US
        birthDate	11/12/1988
        birthCountry	United States
        placeOfBirth	Saint Augustine
        currentCompany	TE2
        jobTitle	SDET
        citizenshipCountryCode	US
        citizenshipCountry	United States
        residencyCountry	United States
        nationality	US
        '''

        payload = '{\n'
        payload += '     "age": "32",\n'
        payload += '     "anniversaryDate": "2015-11-12",\n'
        payload += '     "birthCountryCode": "United States",\n'
        payload += '     "birthDate": "1988-11-12",\n'
        payload += '     "citizenshipCountryCode": "US",\n'
        payload += '     "companyOfEmployment": "Level 11",\n'
        payload += '     "countryOfBirth": "United States",\n'
        payload += '     "countryOfCitizenship": "United States",\n'
        payload += '     "countryOfResidency": "United States",\n'
        payload += '     "firstName": "{fn}",\n'.format(fn=self.__options['first_name'])
        payload += '     "gender": "Male",\n'
        payload += '     "languagePreference": "eng",\n'
        payload += '     "lastName": "{ln}",\n'.format(ln=self.__options['last_name'])
        payload += '     "maritalStatus": "Single",\n'
        payload += '     "middleName": "{g}",\n'.format(g=self.__options['middle'])
        payload += '     "nationalityCode": "US",\n'
        payload += '     "nickName": "Automation",\n'
        # payload += '     "phoneNumbers": "",\n'
        payload += '     "placeOfBirth": "Seattle",\n'
        payload += '     "preferredName": "PoAdult",\n'
        payload += '     "retiredFlag": "N",\n'
        payload += '     "suffix": "Mr",\n'
        payload += '     "title": "SDET",\n'
        payload += '     "wheelchairType": "NotConfined"\n'
        payload += "}"

        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }

        # logthis(url)
        logthis(payload.replace('\\n', ''))

        response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
        # logthis(response)
        logthis(response.text)

        for k, v in response.json().items():
            val = '__' + str(k)
            self.__response[val] = v
            if 'uid' in k:
                self.__captured_uiid = str(v)

    def __link_uiid_to_username(self):
        logthis("\tLinking UIID to Username")
        # url = "http:///%7B%7Bprotocol%7D%7D://%7B%7Bauth-host%7D%7D%7B%7Bcontext%7D%7D%7B%7Bversion%7D%7D/auth-task/oceanId-3a241dd5-f688-4e0f-9bd9-717e6b6e9326/accounts/%7B%7Bemail%7D%7D"
        if len(self.__context) > 1:
            url = self.url.replace(self.__openam_host, self.__auth_host) + \
                  self.__context + "/" + self.__version + '/auth-task/'
        else:
            url = self.url.replace(self.__openam_host, self.__auth_host) + \
                  self.__version + '/auth-task/'

        if self.__captured_uiid:
            url += str(self.__captured_uiid) + '/accounts/' + self.__email

            headers = {
                'content-type': "application/json",
                'cache-control': "no-cache",
            }

            response = requests.request("POST", url, headers=headers)
            logthis(response.json())
        else:
            logthis("ERROR in capturing uiid")

    def __create_ccn_to_username_link(self):
        logthis("\tCreating CCN to Username link")
        # url = "http:///%7B%7Bprotocol%7D%7D://%7B%7Bidentity-host%7D%7D%7B%7Bcontext%7D%7D%7B%7Bversion%7D%7D/identity/user/relation"

        if len(self.__context) > 1:
            url = self.url.replace(self.__openam_host, self.__identity_host) + \
                  self.__context + "/" + self.__version + '/identity/user/relation/'
        else:
            url = self.url.replace(self.__openam_host, self.__identity_host) + \
                  self.__version + '//identity/user/relation/'

        payload = '{\n'
        payload += '     "entity2Id": "{eid}",\n'.format(eid=self.__captured_uiid)
        payload += '     "entity1Id": "{{brandCRMId}}",\n'

        # payload = "{\n\
        # "entity2Id": "oceanId-3a241dd5-f688-4e0f-9bd9-717e6b6e9326",\n\
        # "entity1Id": "{{brandCRMId}}",\n\
        # "entity2Type": "uiid",\n\
        # "entity1Type": "ccn",\n\
        # "certainty": 1,\n\
        # "inferenceSource": "Postman - Account Creation Script",\n\
        # "category": "testing"\n}"
        #
        # {
        # "entity2Id": "oceanId-2123ea91-c0a3-4f4a-9023-9a39ebe6643d",
        # "entity1Id": "po-ccn-10",
        # "entity2Type": "uiid",
        # "entity1Type": "ccn",
        # "certainty": 1,
        # "inferenceSource": "Postman - Account Creation Script",
        # "category": "testing"
        # }

        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }

        # response = requests.request("PUT", url, data=payload, headers=headers)
        #
        # print(response.text)
        #
        #
        #     def 6. Create CCN to Username Link (Two-way)
        # import requests
        #
        # url = "http:///%7B%7Bprotocol%7D%7D://%7B%7Bidentity-host%7D%7D%7B%7Bcontext%7D%7D%7B%7Bversion%7D%7D/identity/user/relation"
        #
        # payload = "{\n"entity1Id": "oceanId-3a241dd5-f688-4e0f-9bd9-717e6b6e9326",\n"entity2Id": "{{brandCRMId}}",\n"entity1Type": "uiid",\n"entity2Type": "ccn",\n"certainty": 1,\n"inferenceSource": "Postman - Account Creation Script",\n"category": "testing"\n}"
        # headers = {
        #     'content-type': "application/json",
        #     'cache-control': "no-cache",
        #     'po
        # response = requests.request("PUT", url, data=payload, headers=headers)
        #
        # print(response.text)


class PeoplePicker:
    ''' Class for testing people-controller '''

    def __init__(self, env='devv', get_bulk_data=False):
        self.url = "http://peoplepicker-svc_8088.{env}.gxicloud.com:9999".format(env=env)
        self.env = env
        self.headers = {
            'accept': "application/json",
            'cache-control': "no-cache",
        }

        if get_bulk_data:
            self.raw_user_list = self.mine_bulk_users()
            self.user_list = self.filter_users()
            logthis('Bulk Data Received')
            logthis(pprint.pformat(self.raw_user_list))
        else:
            self.raw_user_list = None

    def is_up(self):
        response = requests.get(url=self.url + "/status")
        logthis(response + response.json())
        return self.__robot_response(response)

    def __robot_response(self, response):
        pretty = ''
        try:
            pretty = pprint.pformat(response.json()['data'])
        except KeyError:
            pretty = pprint.pformat(response.json())
        finally:
            logthis(pretty)
        return response.status_code, response.json(), pretty

    def __set_url(self, append_url):
        return ''.join([self.url, append_url])

    def mine_bulk_users(self):
        bulk_data_file = self.env + "_bulk_data.json"
        logthis(bulk_data_file)

        a_day_ago = 0
        filetime = 0
        try:
            a_day_ago = datetime.datetime.now() - datetime.timedelta(days=1)
            filetime = datetime.datetime.fromtimestamp(path.getctime(bulk_data_file))

            logthis(filetime)
            logthis(a_day_ago)
            if filetime < a_day_ago:
                logthis("Using cached bulk data")
                with open(bulk_data_file, 'r') as json_data:
                    d = json.load(json_data)
            return d
        except Exception as e:
            # https://guest-task-devint.gxicloud.com:8443/v1/users/bulk
            url = ''.join(["https://guest-task-", self.env, ".gxicloud.com:8443/v1/users/bulk"])
            logthis("URL used: " + url)
            response = requests.get(url=url)
            with open(bulk_data_file, 'w') as outfile:
                json.dump(response.json(), outfile)
            return response.json()

    def filter_users(self):
        user_list = []
        if self.raw_user_list is not None:
            for us in self.raw_user_list:
                if us['personalDetails']:
                    f = us['personalDetails']['firstName'] if us['personalDetails']['firstName'] is not '' else ''
                    m = us['personalDetails']['middleName'] if us['personalDetails']['middleName'] is not '' else ''
                    l = us['personalDetails']['lastName'] if us['personalDetails']['lastName'] is not '' else ''
                    if None not in [f, m, l]:
                        user_list.append([f, m, l, us['userId']])
        pretty = pprint.pformat(user_list)
        logthis("Filtered User list: " + pretty)
        return user_list

    def getUserDetailsUsingGET(self, uiid):
        url = self.__set_url('/v1/social/pp/users/details')
        querystring = {"user-ids": "{}".format(uiid)}
        response = requests.request("GET", url, headers=self.headers, params=querystring)
        return self.__robot_response(response)

    def getAllUsersUsingGET(self):
        url = self.__set_url('/v1/social/pp/users')
        logthis("URL used: " + url)
        response = requests.request("GET", url, headers=self.headers)
        return self.__robot_response(response)

    def getMedallionUsersUsingGET(self, medallions='abc'):
        url = self.__set_url('/v1/social/pp/users/medallions')
        logthis("URL used: " + url)
        querystring = {"medallions": "{meds}".format(meds=medallions)}
        response = requests.request("GET", url, headers=self.headers, params=querystring)
        return self.__robot_response(response)

    def searchUsingGET(self, search_string='Otto Mation'):
        url = self.__set_url('/v1/social/pp/users/search')
        logthis("URL used: " + url)
        querystring = {"name": "{ss}".format(ss=search_string)}
        response = requests.request("GET", url, headers=self.headers, params=querystring)
        return self.__robot_response(response)


def new_people_picker(environment, bulk_data=False):
    return PeoplePicker(env=environment, get_bulk_data=bulk_data)


def create_new_user(verify=False, **kwargs):
    cu = CreateUser(kwargs=kwargs)
    cu.execute()
    logthis("Newly created user details:" + str(cu))
    logthis("Created User UIID: " + cu.uiid)
    if verify:
        logthis(cu.options)
        response = people_controller_getUserDetailsUsingGET(environ=cu.options['env'], ocean_id=cu.uiid)
        if isinstance(response, collections.Iterable):
            logthis(response.text)
        else:
            logthis(response)
        if cu.uiid not in response.text:
            raise ExecutionFailed(message="UIID was not found in People Picker response")
        else:
            logthis("Ocean ID found in People Picker")
    return cu


def __people_picker_assertion(guest, guest_assertion_item, people_picker_assertion_item):
    rv = people_controller_getUserDetailsUsingGET(environ=environment, ocean_id=guest['guest-id'])
    logthis(guest[guest_assertion_item])
    logthis(rv.json()['data'][0][people_picker_assertion_item])
    assert guest[guest_assertion_item] == rv.json()['data'][0][people_picker_assertion_item]


def verify_guest_ID_matches_in_people_picker(guest):
    __people_picker_assertion(guest, "guest-id", 'userId')


def verify_guest_first_name_matches_in_people_picker(guest):
    __people_picker_assertion(guest, "first-name", 'firstName')


def verify_guest_last_name_matches_in_people_picker(guest):
    __people_picker_assertion(guest, "last-name", 'lastName')


def people_controller_getUserDetailsUsingGET(environ="devv", ocean_id=None):
    if ocean_id is not None:
        url = "http://peoplepicker-svc_8088.{env}.gxicloud.com:9999/v1/social/pp/users/details".\
            format(env=environ).\
            replace('%22', '')
        querystring = {"user-ids": "{}".format(ocean_id)}

        headers = {
            'accept': "application/json",
            'cache-control': "no-cache",
        }
        response = requests.request("GET", url, headers=headers, params=querystring)
        logthis(response.json())
        return response
    else:
        return "Ocean ID needs to be supplied"


def verify_people_picker_response_data(environ, ocean_id, k, v):
    r = people_controller_getUserDetailsUsingGET(environ, ocean_id)
    if r.json()['data'][0][k] == v:
        logthis(r.json())
        return True
    else:
        raise ExecutionFailed(message='{k}: {v} was not found in {pr}'.format(k=k, v=v, pr=pprint.pformat(r)))

