# -*- coding: utf-8 -*-

import os
import platform
import sys
import time
import pprint
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import json

from PIL import Image
from collections import OrderedDict
from shutil import copyfile
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options

guests = [
    {
        "id": 3,
        "title": "Dr.",
        "first_name": "Helaine",
        "middle_name": "Berthe",
        "last_name": "McTear",
        "suffix": "Sr",
        "gender": "Female",
        "birthdate": "12/01/1981",
        "address": "5670 Trailsway Lane",
        "city": "Henderson",
        "state": "Nevada",
        "postal_code": "89074",
        "primary_phone": "7026970295",
        "phone_type": "Home",
        "email": "Helaine.McTear@gxitestmail.com"
    },
    {
        "id": 4,
        "title": "Master",
        "first_name": "Conrado",
        "middle_name": "Hebert",
        "last_name": "Whiskin",
        "suffix": "",
        "gender": "Male",
        "birthdate": "04/25/1991",
        "address": "870 Jana Crossing",
        "city": "West Palm Beach",
        "state": "Florida",
        "postal_code": "33421",
        "primary_phone": "5611439055",
        "phone_type": "Home",
        "email": "Conrado.Whiskin@gxitestmail.com"
    },
    {
        "id": 5,
        "title": "Miss",
        "first_name": "Ianthe",
        "middle_name": "Bonita",
        "last_name": "Berrill",
        "suffix": "",
        "gender": "Female",
        "birthdate": "06/03/1977",
        "address": "998 Warrior Road",
        "city": "Detroit",
        "state": "Michigan",
        "postal_code": "48275",
        "primary_phone": "3139240775",
        "phone_type": "Home",
        "email": "Ianthe.Berrill@gxitestmail.com"
    }
]


def put_phantom_driver_in_path():
    for dir_name in os.listdir(os.path.abspath('phantomDrivers')):
        if platform.uname()[0].lower() in dir_name:
            exe = 'phantomjs.exe' if 'win' in platform.uname()[0].lower() else 'phantomjs'
            destination_file = os.sep.join([sys.path[0], exe])
            if not os.path.isfile(destination_file):
                copyfile(os.sep.join([os.path.abspath('phantomDrivers'), dir_name, 'bin', exe]), destination_file)


def put_chrome_driver_in_path():
    for dir_name in os.listdir(os.path.abspath('chromeDrivers')):
        if platform.uname()[0].lower() in dir_name:
            exe = 'chromedriver.exe' if 'win' in platform.uname()[0].lower() else 'chromedriver'
            destination_file = os.sep.join([sys.path[0], exe])
            if not os.path.isfile(destination_file):
                copyfile(os.sep.join([os.path.abspath('chromeDrivers'), dir_name, 'bin', exe]), destination_file)


class Booking:
    '''
        https://carnival.atlassian.net/wiki/display/TPA/How-To%3A+Guest+Booking+on+princess.com
    '''
    environment = 'devint'
    img_count = 0
    save_screenshots = True
    show_images = False
    driver = None
    itineraries = {}
    delay = 60 * 5  # of seconds we wait for objects to be available

    # locators
    viewResults = ['css', '#view-results']
    signupClose = ['css', 'button.mfp-close']
    itineraryResults = ['class', 'result']
    cruiseDetailsButton = ['xpath', '//*[@id="{id}"]//div[@class="cruise-details-btn-container  "]//button']
    selectARoomButton = ["css", "span.xs-hidden.sm-hidden"]
    numberOfGuestsButton = ['xpath', '//button[contains(., "{numberOfGuests}")]']
    i_ll_choose_my_own_button = ['css', '#pick-stateroom']
    quick_stateroom_button = ['css', '#quick-stateroom']
    dining_and_stateroom_continue_button = ['css', 'button.button.green-btn.keep-stateroom']
    bookingID = '//div[@id="popupContent"]//span[@class="font-size-p4 gotham-xn-medium"]'

    def __init__(self, environment=None):
        '''
        initializes the booking page as an object
        '''
        put_phantom_driver_in_path()  # grabs the phantom driver and puts it where it can be used

        desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()

        # deleting the user agent sets the request to a mobile
        desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = \
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' \
            'AppleWebKit/537.36 (KHTML, like Gecko) ' \
            'Chrome/59.0.3071.115 ' \
            'Safari/537.36'

        # mod-header hack required by trident project to see the site
        desired_capabilities['phantomjs.page.customHeaders.STG-ACC'] = '25JP'
        desired_capabilities['phantomjs.page.customHeaders.STG-ACC-ADB1'] = '63XZ'
        desired_capabilities['elementScrollBehavior'] = '1'

        self.driver = webdriver.PhantomJS(desired_capabilities=desired_capabilities)

        # just so we can see the elements on the screen properly
        self.driver.set_window_size(1400, 1000)

        if environment is not None:
            self.environment = environment

        env_url = "tst" if 'dev' in self.environment else "qa"
        self.driver.get("https://{env}www.princess.com/".format(env=env_url))
        # pprint.pprint(self.driver.current_url)
        self.__ss('open_booking')

    def __enter__(self):
        ''' returns object'''
        return self

    def __source(self):
        '''
        html page source
        :return: page source
        '''
        return self.driver.page_source

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''
        tear down method, closes phantom driver
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return: n/a
        '''
        time.sleep(10)
        self.__ss("final_screen")
        with open(str(self.img_count) + 'Bookings_final.html', 'w') as out_file:
            out_file.write(self.__source().encode('utf8'))

        self.driver.close()

    def __ss(self, ssname, element=None):
        '''
        internal mechanism for taking screen shots along the way
        :param ssname: name of screen shot
        :param element: element to be screenshotted
        :return: n/a
        '''
        # assign a name to the output file
        self.img_count += 1
        im_name = '.'.join([str(self.img_count), "Phantom", ssname, 'png'])

        if element is None:  # if screenshotting the entire page
            self.driver.save_screenshot(im_name)

        else:  # screenshotting a specific element on the page
            location = element.location
            size = element.size
            self.driver.save_screenshot(im_name)  # saves screen shot of entire page
            im = Image.open(im_name)  # uses PIL library to open image in memory

            # determine the size of the element to be screenshotted
            left = location['x']
            top = location['y']
            right = location['x'] + size['width']
            bottom = location['y'] + size['height']
            # crop the image
            im = im.crop((left, top, right, bottom))  # defines crop points
            im.save(im_name)  # saves new cropped image

        pim_name = os.path.abspath(im_name)
        print(pim_name)
        if self.show_images:
            imgplot = plt.imshow(mpimg.imread(pim_name))
            plt.ion()
            plt.show()
            time.sleep(5)
            plt.close()
            imgplot = None

        if not self.save_screenshots:
            os.remove(im_name)

    def click(self, elem=None):
        '''
        clicks the element given a list object that defines it
        :param elem: locator type, locator i.e.: ["css", "#my_id"]
        :return: n/a
        '''
        info = ''.join(['Click method ', str(elem)])
        sc = set(['[', ']', "'", '#', ',', '.'])

        # print(info)
        self.driver.set_window_size(100, 100)
        time.sleep(.5)
        self.driver.set_window_size(1400, 1000)
        time.sleep(.5)

        if elem is not None:
            if elem[0] == 'css':
                # elements = self.driver.find_elements_by_css_selector(elem[1])
                self.wait_for_elem(elem[1], By.CSS_SELECTOR)
                click_this = self.driver.find_element_by_css_selector(elem[1])
            elif elem[0] == 'xpath':
                # elements = self.driver.find_elements_by_xpath(elem[1])
                self.wait_for_elem(elem[1], By.XPATH)
                click_this = self.driver.find_element_by_xpath(elem[1])
            elif elem[0] == 'id':
                # elements = self.driver.find_elements_by_id(elem[1])
                self.wait_for_elem(elem[1], By.ID)
                click_this = self.driver.find_element_by_id(elem[1])

            try:
                click_this.click()
                time.sleep(1)
            except Exception as e:
                print (e)

        self.__ss(''.join([c for c in str(elem) if c not in sc]))

    def __get_items(self, elem=None):
        '''
        gets a collection of elements based on the locator provided
        :param elem:
        :return: list of elements
        '''
        if elem is not None:
            if elem[0] == 'class':
                return self.driver.find_elements_by_class_name(elem[1])

    def __get_item(self, elem=None):
        '''
        gets an element on the screen
        :param elem:
        :return: element
        '''
        if elem is not None:
            if elem[0] == 'class':
                return self.driver.find_element_by_class_name(elem[1])
            if elem[0] == 'id':
                return self.driver.find_element_by_id(elem[1])
            if elem[0] == 'xpath':
                return self.driver.find_element_by_xpath(elem[1])

    def exist(self, elem=None):
        '''
        determines if an element exists
        :param elem:
        :return: boolean
        '''
        if elem is not None:
            if elem[0] == 'css':
                return True if self.driver.find_elements_by_css_selector(elem[1]) else False
            if elem[0] == 'id':
                return True if self.driver.find_elements_by_id(elem[1]) else False

    def wait_for_elem(self, elem_locator, by):
        '''
        waits for an element specified to be present
        :param elem_locator:
        :param by:
        :return:
        '''
        print('Waiting for element: {el}, {loc}'.format(el=str(elem_locator), loc=str(by)))
        try:
            myElem = WebDriverWait(self.driver, self.delay).until(
                EC.presence_of_element_located((by, elem_locator)))
            self.driver.execute_script("arguments[0].scrollIntoView();", myElem)
            # print "Element is ready!"
        except TimeoutException:
            print "Loading took too much time!"
            # try:
            #     print(self.__source().replace('><', '>\n<'))
            # except UnicodeEncodeError as e:
            #     print(self.__source())

    def find_a_cruise(self, month=None, destination=None, port=None, duration=None, signup_for_email=False):
        '''
        functional method to search for a given cruise
        :param month:
        :param destination:
        :param port:
        :param duration:
        :param signup_for_email:
        :return: n/a
        '''
        cruise = {"month": month,
                  "destination": destination,
                  'port': port,
                  'duration': duration}

        for k, v in cruise.items():
            if v is not None:
                self.wait_for_elem(k, By.ID)
                # print("//*[@id='{id}']/option[text()='{selection}']".format(id=k, selection=v))
                self.driver.find_element_by_xpath(
                    "//*[@id='{id}']/option[text()='{selection}']".format(id=k, selection=v)
                ).click()

        self.click(self.viewResults)

        if not signup_for_email:
            if self.exist(self.signupClose):
                self.click(self.signupClose)
        self.__ss('find_results')

        # self.itineraries = self.__get_items(self.itineraryResults)
        for itinerary in self.__get_items(self.itineraryResults):
            # print('*' * 80)
            itin_id = itinerary.get_attribute('id')
            # pprint.pprint(itin_id)
            self.__ss(itin_id, element=itinerary)

            # print('*' * 80)
            itin_title = self.__get_item(['id', 'itin-title-' + str(itin_id)]).text
            # print(itin_title)
            self.itineraries[itin_title] = [itin_id, itinerary]
            # pprint.pprint(self.itineraries)

    def cruise_details(self, cruise_title):
        # //*[@id="{id}"]//div[@class="cruise-details-btn-container  "]//button
        cruise_id = self.itineraries[cruise_title][0]
        self.wait_for_elem(cruise_id, By.ID)
        self.__get_item(['xpath', self.cruiseDetailsButton[1].format(id=cruise_id)]).click()
        self.__ss(cruise_title)

    def select_a_room(self):
        self.click(self.selectARoomButton)
        self.__ss("Stateroom Selection")

    def number_of_guests_selection(self, number_of_guests):
        guest_button = self.numberOfGuestsButton
        guest_button[1] = guest_button[1].format(numberOfGuests=str(number_of_guests))
        self.click(guest_button)
        self.__ss("Number of Guests Selected")

    def stateroom_type_selection(self, stateroomType):
        '''
        type of stateroom the travel party will choose
        :param stateroomType: one of Suite, Mini-Suite, Balcony, Interior
        :return: n/a
        '''
        # print(stateroomType)
        button = "#" + stateroomType + " > button.col-stateroom-inner.light-border"
        self.wait_for_elem("#" + stateroomType, By.CSS_SELECTOR)
        self.click(['css', button])
        self.__ss('Stateroom type')

    def stateroom_options_selection(self, stateroomOptions):
        # print(stateroomOptions)
        rows = 0
        while rows < 1:
            time.sleep(1)
            rows = self.driver.find_elements_by_css_selector('div.meta-row')
        self.wait_for_elem(stateroomOptions, By.ID)
        button = '//*[contains(@id, "{id}")]//button'.format(id=stateroomOptions)
        # self.wait_for_elem(stateroomOptions, By.ID)
        self.wait_for_elem(button, By.XPATH)
        #  #BU > div:nth-of-type(2) > div:nth-of-type(2) > button.button.green-btn.gotham-xn-medium
        self.click(['xpath', button])
        self.__ss('Stateroom Options')

    def choose_stateroom_location_choice(self, choice):
        # print(choice)
        time.sleep(3)
        button = self.i_ll_choose_my_own_button if str(choice).lower().startswith('i') else self.quick_stateroom_button
        self.wait_for_elem(button[1], By.CSS_SELECTOR)
        self.click(button)
        # what happens if the stateroom choice is full?
        # Penthouse Suite Staterooms are currently SOLD OUT. Please select another stateroom type.
        self.__ss("Stateroom choice")

    def continue_dining_and_stateroom_details(self):
        self.click(self.dining_and_stateroom_continue_button)

    def fill_guests(self, guests):
        '''
        there are a series of fields that need to be filled on the page that are
        the details of a guest.  some fields are dropdown, other are text, etc.
        :param guests:
        :return:
        '''
        fields = OrderedDict()
        fields["title"] = ['css', '#pax{n}_title', 4]
        fields["first_name"] = ['css', '#pax{n}_firstName', 4]  # pax2_firstName
        fields["middle_name"] = ['css', '#pax{n}_middleName', 4]
        fields["last_name"] = ['css', '#pax{n}_lastName', 4]
        fields["suffix"] = ['css', '#pax{n}_suffix', 4]
        fields["gender"] = ['css', '#pax{n}_gender', 4]
        fields["birth_month"] = ['css', '#pax{n}_birthMonth', 4]
        fields["birth_date"] = ['css', '#pax{n}_birthDate', 4]
        fields["birth_year"] = ['css', '#pax{n}_birthYear', 4]

        fields["address"] = ['css', '#pax{n}_address1', 1]
        # fields["skip_lookup"] = ['css', '#pax1-skip-address-lookup-btn', 1]
        fields["city"] = ['css', '#pax1_city', 1]
        fields["state"] = ['css', '#pax1_stateselect', 1]
        fields["postal_code"] = ['css', '#pax1_postalCode', 1]

        fields["primary_phone"] = ['css', '#pax{n}_phone', 1]
        fields["phone_type"] = ['css', '#pax{n}_phoneType', 1]
        # fields["phone_type"] = ['css', 'select[id$="_phoneType"]', 1]
        fields["email"] = ['css', '#pax{n}_email', 1]
        fields["confirm_email"] = ['css', '#pax{n}_confirmEmail', 1]
        # fields["next_passenger_button"] = ['css', '#next-passenger-{n}', 4]

        # the special_fields list separates out fields that need special handling
        special_fields = ['skip_lookup', 'birth_month', 'birth_date', 'birth_year', 'email', 'confirm_email']

        # dropdowns list separates out the dropdown fields
        dropdowns = ['title', 'suffix', 'gender', 'phone_type']

        # there will be a list of guests passed in, and based on the len of the list we will
        # fill out the details for each guest
        # the guest(s) keys will need to match the fields
        for i in range(1, len(guests) + 1):
            for field, elem in fields.iteritems():
                # handle fields that are not in the special fields list (default case)
                if i <= elem[2] and field not in special_fields:
                    # print(field, guests[i - 1][field], elem[0], elem[1])
                    if guests[i - 1][field] is not '':
                        # print("Looking for: " + elem[1].format(n=i))
                        self.wait_for_elem(elem[1].format(n=i), By.CSS_SELECTOR)

                        if field in dropdowns:
                            # time.sleep(2)
                            # print(elem[1].format(n=i))
                            dd_field = self.driver.find_element_by_css_selector(elem[1].format(n=i))
                            for option in dd_field.find_elements_by_tag_name('option'):
                                # print(option.text)
                                if option.text == guests[i - 1][field]:
                                    option.click()  # select() in earlier versions of webdriver
                                    self.__ss(guests[i - 1][field])
                                    break

                        # fill out the plain old text fields
                        else:
                            if field == 'address':  # because send keys method will not trigger auto lookup on address
                                self.driver.find_element_by_css_selector('#pax1-skip-address-lookup-btn').click()
                            text_field = self.driver.find_element_by_css_selector(elem[1].format(n=i))
                            text_field.send_keys(guests[i - 1][field])
                            self.__ss(guests[i - 1][field])

                # now let's handle some special fields
                elif i <= elem[2] and 'birth' in field:
                    # print(field)
                    if field == 'birth_month':
                        bdv = guests[i - 1]["birthdate"].split("/")[0]
                    elif field == "birth_date":
                        bdv = guests[i - 1]["birthdate"].split("/")[1]
                    elif field == "birth_year":
                        bdv = guests[i - 1]["birthdate"].split("/")[2]
                    text_field = self.driver.find_element_by_css_selector(elem[1].format(n=i))
                    text_field.send_keys(bdv)
                    self.__ss(field)

                elif field == 'email':  # handle this field in confirm_email block
                    pass
                elif i <= elem[2] and field == 'confirm_email':
                    email = guests[i - 1]['email']
                    email_field = self.driver.find_element_by_css_selector(elem[1].replace("confirmE", 'e').format(n=i))
                    confirm_email_field = self.driver.find_element_by_css_selector(elem[1].format(n=i))
                    for text_field in [email_field, confirm_email_field]:
                        text_field.send_keys(email)
                    self.__ss('email')

            self.click(['css', '#next-passenger-{n}'.format(n=i)])
            time.sleep(15)
            if self.exist(self.signupClose):      # because there is a known bug and trying again is the workaround
                self.click(self.signupClose)
                self.click(['css', '#next-passenger-{n}'.format(n=i)])
                time.sleep(15)

        self.click(["css", 'button.align-center.button.green-btn'])
        if self.exist(self.signupClose):  # because there is a known bug and trying again is the workaround
            self.click(self.signupClose)
            self.click(["css", 'button.align-center.button.green-btn'])
            time.sleep(15)

        # for i in range(len(guests)):
        #     self.click(['css', '#paxprotection{n}-N'.format(n=i + 1)])
        #
        # self.click(['css', 'button.align-center.button.green-btn'])
        # if self.exist(self.signupClose):  # because there is a known bug and trying again is the workaround
        #     self.click(self.signupClose)
        #     self.click(["css", 'button.align-center.button.green-btn'])
        #     time.sleep(15)

        cc_field = self.driver.find_element_by_css_selector('#cc_nu')
        cc_field.send_keys('4111111111111111')

        cvc = self.driver.find_element_by_css_selector('#cc_cvc')
        cvc.send_keys('123')

        dds = [
            ['paymentVO_ccExpMonth0', 'December'],
            ['paymentVO_ccExpYear0', '2019'],
            ['billing-address', guests[0]['first_name']]
        ]
        # pprint.pprint(self.driver.page_source)
        for dd in dds:
            # print(dd)
            self.wait_for_elem(dd[0], By.ID)
            d_field = self.driver.find_element_by_id(dd[0])
            for option in d_field.find_elements_by_tag_name('option'):
                if dd[1] in option.text:
                    option.click()
                    self.__ss(dd[0])
                    break

        for i in range(len(guests)):
            self.click(['css', '#pax_{n}_accept'.format(n=i + 1)])
            self.__ss('pax_{n}_accept'.format(n=i + 1))

        self.click(['css', '#button-payment'])
        if self.exist(self.signupClose):  # because there is a known bug and trying again is the workaround
            self.click(self.signupClose)
            self.click(['css', '#button-payment'])
            cc_field = self.driver.find_element_by_css_selector('#cc_nu')
            cc_field.send_keys('4111111111111111')

            cvc = self.driver.find_element_by_css_selector('#cc_cvc')
            cvc.send_keys('123')

            dds = [
                ['paymentVO_ccExpMonth0', 'December'],
                ['paymentVO_ccExpYear0', '2019'],
                ['billing-address', guests[0]['first_name']]
            ]
            # pprint.pprint(self.driver.page_source)
            for dd in dds:
                # print(dd)
                self.wait_for_elem(dd[0], By.ID)
                d_field = self.driver.find_element_by_id(dd[0])
                for option in d_field.find_elements_by_tag_name('option'):
                    if dd[1] in option.text:
                        option.click()
                        self.__ss(dd[0])
                        break

            for i in range(len(guests)):
                self.click(['css', '#pax_{n}_accept'.format(n=i + 1)])
                self.__ss('pax_{n}_accept'.format(n=i + 1))

            time.sleep(15)
        self.__ss('button-payment')

        pprint.pprint(self.driver.page_source)
        bookingId = self.driver.find_element_by_xpath(self.bookingID).text

        return bookingId

    def add_guests_to_room(self, guests, stateroomType, stateroomOptions, roomLocationChoice):
        self.number_of_guests_selection(len(guests))
        self.stateroom_type_selection(stateroomType)
        self.stateroom_options_selection(stateroomOptions)
        self.choose_stateroom_location_choice(roomLocationChoice)
        self.continue_dining_and_stateroom_details()
        bookingID = self.fill_guests(guests)
        return bookingID


class Ocean:
    img_count = 0
    save_screenshots = True
    show_images = False
    delay = 60 * 5  # of seconds we wait for objects to be available
    oceanUrl = 'https://ocean-compass-web-devint.gxicloud.com:8443/compass'
    join_ocean_button = ['css', 'button.common-buttons__button___NCduV.common-buttons__formButton___TSoZS']
    # generic_ocean_button = ['css', 'button[class*="common-buttons"]']
    generic_ocean_button = ['css', 'button']
    join_first_name_field = ['css', 'div > div:nth-of-type(3) > div > input']
    join_last_name_field = ['css', 'div > div:nth-of-type(4) > div > input']
    join_email_field = ['css', 'div > div:nth-of-type(5) > div > input']
    join_button = ['css', 'button']
    password_field = ['css', '#password']
    confirm_password_field = ['css', '#confirm_password']

    def __init__(self, environment=None):
        '''
        initializes the booking page as an object
        '''
        put_chrome_driver_in_path()

        chrome_options = Options()
        chrome_options.add_argument("--disable-infobars")
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

        # just so we can see the elements on the screen properly
        self.driver.set_window_size(1400, 1000)

        if environment is not None:
            self.environment = environment

        self.driver.get(self.oceanUrl)

        # pprint.pprint(self.driver.current_url)
        self.__ss('Open_Ocean')

    def __enter__(self):
        ''' returns object'''
        return self

    def __source(self):
        '''
        html page source
        :return: page source
        '''
        return self.driver.page_source

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''
        tear down method, closes phantom driver
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return: n/a
        '''
        time.sleep(10)
        self.__ss("final_screen")
        with open(str(self.img_count) + 'Ocean_final.html', 'w') as out_file:
            out_file.write(self.__source().encode('utf8'))
        print (self.__source())
        self.driver.close()

    def __ss(self, ssname, element=None):
        '''
        internal mechanism for taking screen shots along the way
        :param ssname: name of screen shot
        :param element: element to be screenshotted
        :return: n/a
        '''
        # assign a name to the output file
        self.img_count += 1

        sc = set(['[', ']', "'", '#', ',', '.', "*", '"', '='])

        ss_name = ''.join([c for c in str(ssname) if c not in sc])

        im_name = '.'.join([str(self.img_count), "Chrome", ss_name, 'png'])

        if element is None:  # if screenshotting the entire page
            self.driver.save_screenshot(im_name)

        else:  # screenshotting a specific element on the page
            location = element.location
            size = element.size
            self.driver.save_screenshot(im_name)  # saves screen shot of entire page
            im = Image.open(im_name)  # uses PIL library to open image in memory

            # determine the size of the element to be screenshotted
            left = location['x']
            top = location['y']
            right = location['x'] + size['width']
            bottom = location['y'] + size['height']
            # crop the image
            im = im.crop((left, top, right, bottom))  # defines crop points
            im.save(im_name)  # saves new cropped image

        pim_name = os.path.abspath(im_name)
        print(pim_name)
        if self.show_images:
            imgplot = plt.imshow(mpimg.imread(pim_name))
            plt.ion()
            plt.show()
            time.sleep(5)
            plt.close()
            imgplot = None

        if not self.save_screenshots:
            os.remove(im_name)

    def __get_items(self, elem=None):
        '''
        gets a collection of elements based on the locator provided
        :param elem:
        :return: list of elements
        '''
        if elem is not None:
            if elem[0] == 'class':
                return self.driver.find_elements_by_class_name(elem[1])

    def __get_item(self, elem=None):
        '''
        gets an element on the screen
        :param elem:
        :return: element
        '''
        if elem is not None:
            if elem[0] == 'class':
                return self.driver.find_element_by_class_name(elem[1])
            if elem[0] == 'id':
                return self.driver.find_element_by_id(elem[1])
            if elem[0] == 'xpath':
                return self.driver.find_element_by_xpath(elem[1])

    def exist(self, elem=None):
        '''
        determines if an element exists
        :param elem:
        :return: boolean
        '''
        if elem is not None:
            if elem[0] == 'css':
                return True if self.driver.find_elements_by_css_selector(elem[1]) else False
            if elem[0] == 'id':
                return True if self.driver.find_elements_by_id(elem[1]) else False

    def click(self, elem=None):
        '''
        clicks the element given a list object that defines it
        :param elem: locator type, locator i.e.: ["css", "#my_id"]
        :return: n/a
        '''
        info = ''.join(['Click method ', str(elem)])
        sc = set(['[', ']', "'", '#', ',', '.'])

        if elem is not None:
            if elem[0] == 'css':
                self.wait_for_elem(elem[1], By.CSS_SELECTOR)
                click_this = self.driver.find_element_by_css_selector(elem[1])
            elif elem[0] == 'xpath':
                self.wait_for_elem(elem[1], By.XPATH)
                click_this = self.driver.find_element_by_xpath(elem[1])
            elif elem[0] == 'id':
                self.wait_for_elem(elem[1], By.ID)
                click_this = self.driver.find_element_by_id(elem[1])

            try:
                click_this.click()
                time.sleep(1)
            except Exception as e:
                print (e)

        self.__ss(''.join([c for c in str(elem) if c not in sc]))

    def type(self, elem=None, text=None):
        '''
        Enters test into the element given a list object that defines it
        :param elem: locator type, locator i.e.: ["css", "#my_id"]
        :return: n/a
        '''
        info = ''.join(['Click method ', str(elem)])

        if elem is not None:
            if elem[0] == 'css':
                self.wait_for_elem(elem[1], By.CSS_SELECTOR)
                click_this = self.driver.find_element_by_css_selector(elem[1])
            elif elem[0] == 'xpath':
                self.wait_for_elem(elem[1], By.XPATH)
                click_this = self.driver.find_element_by_xpath(elem[1])
            elif elem[0] == 'id':
                self.wait_for_elem(elem[1], By.ID)
                click_this = self.driver.find_element_by_id(elem[1])

            time.sleep(1)
            try:
                click_this.click()
                time.sleep(1)
                click_this.send_keys(text)
            except Exception as e:
                print (e)

        self.__ss(str(elem))

    def wait_for_elem(self, elem_locator, by):
        '''
        waits for an element specified to be present
        :param elem_locator:
        :param by:
        :return:
        '''
        # print('Waiting for element: {el}, {loc}'.format(el=str(elem_locator), loc=str(by)))
        try:
            myElem = WebDriverWait(self.driver, self.delay).until(
                EC.presence_of_element_located((by, elem_locator)))
            self.driver.execute_script("arguments[0].scrollIntoView();", myElem)
            # print "Element is ready!"
        except TimeoutException:
            print "Loading took too much time!"
            try:
                print(self.__source().replace('><', '>\n<'))
            except UnicodeEncodeError as e:
                print(self.__source())

    def show_guests(self, guests):
        for guest in guests:
            pprint.pprint(guest)

    def join_ocean(self, guest, bookingID):
        self.click(self.join_ocean_button)
        self.type(self.join_first_name_field, guest['first_name'])
        self.type(self.join_last_name_field, guest['last_name'])
        self.type(self.join_email_field, guest['email'])
        time.sleep(2)
        self.click(self.generic_ocean_button)
        time.sleep(5)
        self.type(self.password_field, 'Welcome1')

        if self.exist(self.confirm_password_field):
            self.type(self.confirm_password_field, 'Welcome1')
            time.sleep(2)
        self.click(self.generic_ocean_button)
        time.sleep(5)


if __name__ == "__main__":
    with Booking() as bs:
        # bs.find_a_cruise(month="August 2017")
        # bs.find_a_cruise(month="August 2017", destination='Caribbean')
        # bs.find_a_cruise(month="August 2017", destination='Caribbean', port='Ft. Lauderdale, Florida')
        # bs.find_a_cruise(month="August 2017", destination='Caribbean', port='Ft. Lauderdale, Florida', duration='9 - 15 Days')

        bs.find_a_cruise(month="September 2017", port='Ft. Lauderdale, Florida')
        bs.cruise_details('Eastern Caribbean')
        bs.select_a_room()
        bookingID = bs.add_guests_to_room(guests, "Interior", "BP", "c")
        print(bookingID)

    # with Ocean() as o:
    #     # o.show_guests(guests=guests)
    #     o.join_ocean(guest=guests[0], bookingID=bookingID)
