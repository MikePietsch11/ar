*** Settings ***
Documentation       Smoke Tests for People Picker service
Resource            ../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test


########################################################################################################################
*** Test Cases ***
########################################################################################################################


########################################################################################################################
Check People Picker status
    [Tags]  people picker
    [Documentation]  Checks to see that the People Picker service does not return an http 5xx error
    ${pp}=  new people picker  ${env}
#    log to console  ${pp}
    ${up}=  Call Method  ${pp}  is_up
#    log to console  ${up}
    ${sc}=  convert to string  ${up[0]}
#    log to console  ${sc}
    should not start with  ${sc}  5
