*** Settings ***
Documentation       People Picker tests
Resource            ../../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test


########################################################################################################################
*** Variables ***
${test_env}=  ${env}
########################################################################################################################


########################################################################################################################
*** Test Cases ***
########################################################################################################################


########################################################################################################################
getUserDetailsUsingGET status code 200
    ${te}  new people picker  ${test_env}  ${True}

    :FOR  ${user}  IN  @{te.user_list[1]}
    \   Log To Console  ${user}
    \   ${un}=  Catenate  ${user[0]}  ${user[1]}  ${user[2]}
    \   ${response}=  Call Method  ${te}  getUserDetailsUsingGET  ${user[3]}
    \   Run Keyword And Continue On Failure  verify response is 200  ${response[0]}


########################################################################################################################
#getUserDetailsUsingGET JSON Contains First Name
#    ${te}  new people picker  ${test_env}  ${True}
#
#    :FOR  ${user}  IN  @{te.user_list}
#    \   Log To Console  ${user}
#    \   ${un}=  Catenate  ${user[0]}  ${user[1]}  ${user[2]}
#    \   ${response}=  Call Method  ${te}  getUserDetailsUsingGET  ${user[3]}
#    \   Run Keyword And Continue On Failure  verify response contains  ${response[2]}  ${user[0]}


########################################################################################################################
#getUserDetailsUsingGET JSON Contains Middle Name
#    ${te}  new people picker  ${test_env}  ${True}
#
#    :FOR  ${user}  IN  @{te.user_list}
#    \   Log To Console  ${user}
#    \   ${un}=  Catenate  ${user[0]}  ${user[1]}  ${user[2]}
#    \   ${response}=  Call Method  ${te}  getUserDetailsUsingGET  ${user[3]}
#    \   Run Keyword And Continue On Failure  verify response contains  ${response[2]}  ${user[1]}


########################################################################################################################
#getUserDetailsUsingGET JSON Contains Last Name
#    ${te}  new people picker  ${test_env}  ${True}
#
#    :FOR  ${user}  IN  @{te.user_list}
#    \   Log To Console  ${user}
#    \   ${un}=  Catenate  ${user[0]}  ${user[1]}  ${user[2]}
#    \   ${response}=  Call Method  ${te}  getUserDetailsUsingGET  ${user[3]}
#    \   Run Keyword And Continue On Failure  verify response contains  ${response[2]}  ${user[2]}

