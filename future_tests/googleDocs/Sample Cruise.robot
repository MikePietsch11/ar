*** Settings ***
Documentation       Sample Cruise created by Nate Wearin
...  https://docs.google.com/spreadsheets/d/1xc9O58EiKIUwX0bzxFELQA8HVrSqxQ-oHuoSAFXtXck/edit#gid=0

Resource            ../../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test

Force Tags          Sample Cruise



########################################################################################################################
*** Test Cases ***
########################################################################################################################


Travel Party Checks In
    [Tags]  not implemented  Sample Cruise
    [Documentation]  Simulates the travel party checking in
    ${travel_party}=  Establish Travel Party


Travel Party Completes Muster Drill
    [Tags]  not implemented  Sample Cruise
    [Documentation]  Simulates the travel party completing the muster drill
    ${travel_party}=  Establish Travel Party
    log to console  ${travel_party}


*** Keywords ***
Establish Travel Party
    [Tags]  not implemented  Sample Cruise
    [Documentation]  Find or Create the travel party that is required for othere tests in this Suite.

    #  will need to fix this
    ${tp}=  convert to string  People Picker
    # First do a search

    # Then if the search doesn't return what we need, go ahead and create it
    [Return]  ${tp}

