*** Settings ***
Documentation       People Picker tests taken from pdf found at
...  https://carnival.atlassian.net/wiki/download/attachments/32768346/people_picker_01.pdf?api=v2

Resource            ../../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test

Force Tags          people_picker_01.pdf


########################################################################################################################
*** Test Cases ***
########################################################################################################################


Event Details
    [Tags]  not implemented
    Fail  Test not implemented
  

Create Personal Event Full Page
    [Tags]  not implemented
    Fail  Test not implemented


Messages View
    [Tags]  not implemented
    Fail  Test not implemented


Share Content
    [Tags]  not implemented
    Fail  Test not implemented


Change Default Guest
    [Tags]  not implemented  Here and Now Then and There
    Fail  Test not implemented


Change Local Guest
    [Tags]  not implemented  Here and Now Then and There
    Fail  Test not implemented


Change Local Guest Identify a Guest for a Specific Duration Add to Cart Customization
    [Tags]  not implemented  Here and Now Then and There
    Fail  Test not implemented


Change Local Guest Identify a Guest for a Specific Item durint the Checkout Flow
    [Tags]  not implemented  Here and Now Then and There
    Fail  Test not implemented


Add New Contact
    [Tags]  not implemented  Guest Pages  Portal
    Fail  Test not implemented
