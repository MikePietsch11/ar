*** Settings ***
Documentation       People Picker tests taken from pdf found at
...  https://carnival.atlassian.net/wiki/download/attachments/32768346/people_picker_02-atomic-elements.pdf?api=v2

Resource            ../../resources/aquanet.robot

Test Setup          Startup Test
Test Teardown       Shutdown Test

Force Tags          atomic-elements.pdf


########################################################################################################################
*** Test Cases ***
########################################################################################################################


Browse
    [Tags]  not implemented
    [Documentation]  System populates a list of people that the guest picks from.
    Fail  Test not implemented


Category Browse
    [Tags]  not implemented
    [Documentation]  A variation of Browse where an explicit query name is exposed
    ...  (eg: nearby, excursions, interests, etc) based on the scenario in
    ...  which it is being used.
    Fail  Test not implemented


Category Browse Excursion Name
    [Tags]  not implemented
    [Documentation]  A variation of Browse where an explicit query name is exposed
    ...  (eg: nearby, excursions, interests, etc) based on the scenario in
    ...  which it is being used.
    Fail  Test not implemented


Category Browse Proximity
    [Tags]  not implemented
    [Documentation]  A variation of Browse where an explicit query name is exposed
    ...  (eg: nearby, excursions, interests, etc) based on the scenario in
    ...  which it is being used.
    Fail  Test not implemented


Category Browse Interests
    [Tags]  not implemented
    [Documentation]  A variation of Browse where an explicit query name is exposed
    ...  (eg: nearby, excursions, interests, etc) based on the scenario in
    ...  which it is being used.
    Fail  Test not implemented


Search
    [Tags]  not implemented
    [Documentation]  System presents a search field for guest to enter name. (TBD: Is
    ...  this only name or can we search other criteria like an excursion name?)
    Fail  Test not implemented

Select
    [Tags]  not implemented
    [Documentation]  Guest picks one person from the list.
    Fail  Test not implemented


Multi Select
    [Tags]  not implemented
    [Documentation]  Guest picks several people from the list.
    Fail  Test not implemented

